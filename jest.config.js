module.exports = {
    "roots": [
        "<rootDir>/lib"
    ],
        "testMatch": [
        "**/__tests__/**/*.+(ts|js)",
        "**/?(*.)+(spec|test).+(ts|js)"
    ],
        "transform": {
        "^.+\\.(ts)?$": "ts-jest"
    }
}